# README #

This README would normally document whatever steps are necessary to get your application up and running.

## Captura de pantalla de la tarea creada ##

![Tarea creada](symfony_apache.png)

## Manual desde donde he realizado la practica ##

[Manual practica](https://www.digitalocean.com/community/tutorials/how-to-deploy-a-symfony-application-to-production-on-ubuntu-14-04)

## Errores encontrados ##

###- Instalar sudoapt-get install php-xml###
###- Cambiar sudo apt-get install git php5-cli php5-curl acl###
###      por sudo apt-get install git php-cli php-curl acl###
###- Cambiar sudo nano /etc/mysql/my.cnf###
###        a sudo nano /etc/mysql/mysql.conf.d/###
###- Cambiar sudo nano /etc/mysql/my.cnf###
###        a sudo nano /etc/mysql/mysql.conf.d/mysqld.cnf###
###- Cambiar todas las rutas /var/www/###
###      por las ruta /var/www/###
###- Cambiar /etc/php5/apache2/php.ini ###
###      por /etc/php/7.0/apache2/php.ini ###
###- En el archivo anterior hay que cambiar date.timezone = Europe/Amsterdam###
###      por date.timezone = Europe/Madrid###
